# website

My personal blog

https://antonioberben.gitlab.io/website

## Development

To run in local:

```sh
docker run --name website --rm -p 1313:1313 -v $(pwd):/src  -w /src klakegg/hugo:0.92.1-ext-alpine server --bind 0.0.0.0
```