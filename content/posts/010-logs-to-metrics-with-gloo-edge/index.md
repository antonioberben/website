---
title: "How to turn application logs into useful metrics with Gloo Edge and Grafana"
date: 2022-02-07

tags: ['k8s', 'promtail', 'prometheus', 'grafana']
author: "Antonio Berben"
---

Published in the Solo.io's official blog:

https://www.solo.io/blog/how-to-turn-application-logs-into-useful-metrics-with-gloo-edge-and-grafana/