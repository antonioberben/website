---
title: "Multiple Identity Providers with Keycloak"
date: 2020-05-01

tags: ['k8s', 'keycloak']
author: "Antonio Berben"
---

Have you ever wondered about how to allow users to login with their google user accounts in your system? And accounts from other Auth Servers?

![](keycloak.png)

Some of our customers have these questions:

> Where are my user details stored? Do I have to register into your system and fulfill a form? How do you secure my personal details?

At my current employer, we gather requirements from other departments based on comments like:

> I have my users' data stored in a database, and I want to use/integrate your services but I don't want you to manage that data.

Or...

> We have our own Auth Server. Our users don’t want to create another account. How can we do SSO to your system?


In this article you will find the step-by-step (with screenshots) tutorial of how to setup a local environment so that you can start discovering yourself how to configure an Auth Server like [Keycloak](https://www.keycloak.org/) to perform [SSO](https://en.wikipedia.org/wiki/Single_sign-on) and use accounts from different providers.

The intention is to show that you can have an Auth Server but no need to own/manage/maintain the Identity of your users.

What you will not find in this article is a deep explanation about SSO and the two protocols ([OIDC - OpenId Connect](https://openid.net/connect/) and [SALM](https://en.wikipedia.org/wiki/SAML_2.0)) offered by the Auth Server ([Keycloak](https://www.keycloak.org/))

The Internet is full of content explaining OIDC. I can give two of my preferences:

- [OAuth and OpenID Connect for Microservices](https://www.youtube.com/watch?v=BdKmZ7mPNns) IMO, one of the best presentations to get you into OIDC

- Auth in one picture <blockquote class="twitter-tweet"><p lang="en" dir="ltr">OAuth in one picture. <a href="https://t.co/udZ2UG8ePx">pic.twitter.com/udZ2UG8ePx</a></p>&mdash; Kamran Ahmed (@kamranahmedse) <a href="https://twitter.com/kamranahmedse/status/1276994010423361540?ref_src=twsrc%5Etfw">June 27, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<br/>
<br/>

---

# Prerequisites

- [Docker](https://docs.docker.com/get-docker/)
- [Docker-compose](https://docs.docker.com/compose/install/)

<br/>
<br/>

---

# Let's get started!

<div style="text-align:center"><img src="./key0.png" alt="alt text" width="800"></div>

You are going to have that design in your local.

You will be able to access to the protected service, using your credentials from your own `google account` or the credentials from the `External Auth Server`

> **Note:** The core concept in Keycloak is a Realm. A realm secures and manages security metadata for a set of users, applications, and registered oauth clients.

<br/>
<br/>

The supplied resources are already ready to be loaded with the Realms, Clients and Identity Providers. However, the one for google, you will have to configure it with your own details.


> **Note:** To start form scratch, you just need to comment out the lines where realms are being imported in the `docker-compose.yaml`.

---

## Local environment

1. Clone the github repository and go to the given folder

    ```sh
    $ git clone git@github.com:antonioberben/examples.git

    $ cd example/keycloak-multiple-id-providers
    ```

1. Start containers

    ```sh
    $ docker-compose up -d
    ```

    > **Hint:** To see the last 10 lines of logs and keep watching the traces:

    ```sh
    $ docker-compose --tail 10 -f
    ```

1. Get the your docker containers gateway IP

    Depending on your host OS, docker creates a network and assigns an Gateway IP to route traffic to your host.

    Figure out the containers name/ID:

    ```sh
    $ docker-compose ps
    ```

    Now figure out the gateway IP:

    ```sh
    $ docker inspect <here container name/id> | grep Gateway 
    ```

1. Adjust for domain resolution

    Since you are in local, you need to simulate two different hostnames. One for `My Auth Server` and the other one for `External Auth Server`

    As you can see in `docker-compose.yaml` file, docker will inject in `/etc/hosts` file for each container, the specifications to route the traffic outside the container. You will work with `my.auth.com` and `external.auth.com`

    In `docker-compose.yaml`:

    ```yaml
    [...]
        extra_hosts:
          my.auth.com: <here the gateway IP>
          external.auth.com: <here the gateway IP>
    [...]
    ```

    Now you will have to add into your own local `/etc/hosts` file the traces, but redirecting to `localhost`/`127.0.0.1`

    > Why? The reason is that the example requires run OIDC flows which requires re-directions from one container to another as well as re-directions from Google to one of the containers. This setup is quite simple, although, if you are familiar with [nip.io](https://nip.io/), it will be better for you. But I did not want to add more complexity.

    In your `/etc/hosts`:

    ```
    # keycloak example
    127.0.0.1 external.auth.com my.auth.com
    ```

1. Stop, remove and start containers again

    ```sh
    $ docker-compose stop

    $ docker-compose rm -f

    $ docker-compose up -d
    ```

After a while, you will be able to access the two different Auth Servers

- My Auth Server
http://my.auth.com:8180/auth

- External Auth Server
http://external.auth.com:8280/auth

<br/>
<br/>

---

## Setup External Auth Server as Identity Provider

Since you want SSO using credentials from different providers, you have to configure both sides.

- In `My Auth Server` you configure one `Identity Provider` for Google and another one for `External Auth Server`.

- In `Google` you configure a `client`

- In `External Auth Server` you configure another client. 

This is the way, though OIDC (OpenID Connect), to let both sides know each other and enable SSO.


1. Go to `External Auth Server` (http://external.auth.com:8280/auth), with credentials:

    ```
    user: admin
    password: admin
    ```

    These credentials are set in `docker-compose.yaml`


1. Create a realm with name `external-auth`


    <div style="text-align:center"><img src="./key1.png" alt="alt text" width="400"></div>
    <br/>
    <br/>


1. Create a client

    <div style="text-align:center"><img src="./key3.png" alt="alt text" width="800"></div>
    <br/>
    <br/>

    It is important to mark the client as `confidential` 

    > Confidential clients are required to provide a client secret when they exchange the temporary codes for tokens

    And the `Valid redirect URIs` needs to point to the `Identity Provider` you are going to set up in parallel, so keep this tab open.

1. Open in another tab `My Auth Server` (http://my.auth.com:8180/auth), with credentials:

    ```
    user: admin
    password: admin
    ```

1. Create a realm with name `my-auth`

1. Setup an Identity Provider which connects to `External Auth Server`


    <div style="text-align:center"><img src="./key4.png" alt="alt text" width="600"></div>

    Here you need several points to pay attention.

    - `Redirect URI` is the attribute you needed from the previous step when you were creating the client
    - `Authorization URL` and `Token URL` are attributes you can find the explanation below
    - `Client ID` and `Client Secret` are attributes you can find from previous step when you created the client in the other server

    > Where do I get `Authorization URL` and `Token URL`?
    >
    >Go to the `External Auth Server`, `external-auth` realm, click on `Realm Settings` and where it says: `Endpoints: OpenID Endpoint Configuration`, it is actually a link. Then, click it and a new tab will be open with the `Discovery Endpoint`.

    - Realm Settings: http://external.auth.com:8280/auth/admin/master/console/#/realms/external-auth


        <div style="text-align:center"><img src="./key5.png" alt="alt text" width="600"></div>

    - Discovery Endpoint: http://external.auth.com:8280/auth/realms/external-auth/.well-known/openid-configuration

        <div style="text-align:center"><img src="./key6.png" alt="alt text" width="600"></div>

1. Save the configurations from both servers.


Now you have configure `External Auth Server` as a `My Auth Server`'s Identity Provider.

<br/>
<br/>

---

## Setup Google as Identity Provider

To set up Google as Identity Provider, follow these steps:

1. Go to [Google Developers Console](https://console.developers.google.com/) and `credentials`


    <div style="text-align:center"><img src="./key7.png" alt="alt text" width="400"></div>

1. Select `OAuth client ID`


    <div style="text-align:center"><img src="./key8.png" alt="alt text" width="400"></div>

    <br/>
    <br/>

    <div style="text-align:center"><img src="./key9.png" alt="alt text" width="400"></div>


    As you can see, in `Authorized redirect URIs` you set the value that you will obtain while configuring the `My Auth Server` side in parallel.

    Find the `client ID` and `client secret` which you will use in the next step

    <div style="text-align:center"><img src="./key10.png" alt="alt text" width="800"></div>

1. In parallel to Google setup, go to `My Auth Server` and create a new Identity Provider. But this time, use one of the options which are offered: `Google`. Configure it as follows:

    <div style="text-align:center"><img src="./key11.png" alt="alt text" width="800"></div>

<br/>
<br/>

---

# Login to Account Service with External Auth Server account

1. Go to [My Auth Server](http://my.auth.com:8180/auth/admin/master/console/#/realms/my-auth/users) and list the users. You will see there are non.

    <div style="text-align:center"><img src="./key12.png" alt="alt text" width="500"></div>

1. Go to [External Auth Server](http://external.auth.com:8280/auth/admin/master/console/#/realms/external-auth/users) and create a user `external-user`. After saving, assign a password: `password`.


    <div style="text-align:center"><img src="./key13.png" alt="alt text" width="500"></div>

    <br/>
    <br/>

    <div style="text-align:center"><img src="./key14.png" alt="alt text" width="500"></div>

1. Try to access the `Account` service which actually is part of keycloak itself (http://my.auth.com:8180/auth/realms/my-auth/account). You will be redirected to the login page where the new Identity Providers are offered. Click on the `External Auth Server` provider

    <div style="text-align:center"><img src="./key15.png" alt="alt text" width="500"></div>

    You will be redirected to the `External Auth Server` login page.

1. Use the credentials you created for `External Auth Server`

    ```
    user: external-user
    password: password
    ```
    <div style="text-align:center"><img src="./key16.png" alt="alt text" width="500"></div>


1. After authentication succeeds, you are back to the `Account` service, logged in with external user credentials

    <div style="text-align:center"><img src="./key17.png" alt="alt text" width="500"></div>


1. Let's check the list of users in `My Auth Server`. You will see now the Identity Image. This means that a copy of the identity from `External Auth Server` has been stored in `My Auth Server`. This gives the capabilities to `My Auth Server` to release access tokens (in a JWT shape) for users which are not actually under its domain.


    <div style="text-align:center"><img src="./key18.png" alt="alt text" width="500"></div>

<br/>
<br/>

---

# Login to Account Service with a Google account


1. Try to access the `Account` service which actually is part of keycloak itself (http://my.auth.com:8180/auth/realms/my-auth/account). You will be redirected to the login page where the new Identity Providers are offered. Click on the `Google` provider

    <div style="text-align:center"><img src="./key19.png" alt="alt text" width="500"></div>

    You will be redirected to the `Google` login page.

    <div style="text-align:center"><img src="./key20.png" alt="alt text" width="500"></div>

1. After authentication succeeds, you are back to the `Account` service, logged in with gmail credentials

    <div style="text-align:center"><img src="./key21.png" alt="alt text" width="500"></div>

1. Like you did before, let's check the list of users in `My Auth Server`. You will see now the Identity Image created for the Google account.


    <div style="text-align:center"><img src="./key22.png" alt="alt text" width="600"></div>

<br/>
<br/>

To see the Identity Provider related to the Identity Image, you can see it at user details, where it says: `Identity Provider Links`.


<div style="text-align:center"><img src="./key23.png" alt="alt text" width="600"></div>

<br/>
<br/>

---

# Last thoughts

Now, you have full control of the Identity's Images, but not of the identities themselves. 

This means that you can release tokens, manage sessions, grant/revoke accesses to your own services, etc. But the Identity of the user stands in another system.


This removes the burden of maintaining the identities from your systems. Your users will not be required to go through a registration process in your system.

As well, it allows you to bring to your systems, users from different buckets (google, external auth server,...)

<br/>
<br/>

---

If you are interested in knowing more deep inside, you can ping me in twitter [@antonio_berben](https://twitter.com/antonio_berben)

<br/>
<br/>

---


