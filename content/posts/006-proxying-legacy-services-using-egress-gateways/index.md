---
title: "Proxying legacy services using Istio egress gateways"
date: 2020-12-16

tags: ['k8s', 'istio', 'mesh']
author: "Antonio Berben"
---

Published in the Istio's official blog:

https://istio.io/latest/blog/2020/proxying-legacy-services-using-egress-gateways