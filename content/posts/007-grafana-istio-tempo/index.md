---
title: "How Istio, Tempo, and Loki speed up debugging for microservices"
date: 2021-08-31

tags: ['k8s', 'istio', 'mesh', 'tracing', 'observability', 'tempo', 'grafana']
author: "Antonio Berben"
---

Published in the Grafana's official blog:

https://grafana.com/blog/2021/08/31/how-istio-tempo-and-loki-speed-up-debugging-for-microservices/