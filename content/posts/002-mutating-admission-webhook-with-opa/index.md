---
title: "Mutating Admission Webhook with OPA"
date: 2020-03-01

tags: ['k8s', 'opa']
author: "Antonio Berben"
---

Wouldn't it be cool that the maintainers of an official helm chart accept your crazy Pull Request even if the change only makes sense to you?

![](opa.png)

Sometimes you need to tweak a chart to satisfy certain needs. The official one does not offer the customization you required. Therefore, you have two options:

1. Open a Pull Request to include the change to the official repository, hoping to be approved ASAP
  
  > But the change is really crazy and only needed by you

2. Fork the chart and start maintaining your own version

  > Are you as lazy as me? 


Today, I will explain to you a third option. It is quick and seamless. And it is an interesting exercise for you to see one of the capabilities [Open Policy Agent](https://www.openpolicyagent.org/) has.

___

# The case

Let me introduce the case to you.

At my current employer, we wanted to integrate [Istio Mesh](https://istio.io/). Through [helm](https://helm.sh/) or the [IstioOperator](https://github.com/istio/istio/tree/master/operator) it is pretty easy. Both are expecting some variables to customize the deployment.

> `Easy, right?`

Due to requirements, we were forced to deploy the `istio-ingressgateway` pods in some specific nodes.


Either the official Helm chart or the IstioOperator offers you that level of customization through [labels in nodes, nodeSelector, taints, affinity and anti-affinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/).

> `Everything looks fine. So, what is the issue?`
___

## And the problem...

Due to our infrastructure, we also needed to expose to `hostNetwork` the ports. In the pod manifest it looks like this:


```yaml
[...]
spec:
  dnsPolicy: ClusterFirstWithHostNet
  hostNetwork: true
[...]
```

And this is neither offered `helm chart` nor the `IstioOperator`.

Besides, there were other more complicated changes I can't share,  which led us to avoid the update in the chart.

> `What a pain!`

Then, how can we still use the helm chart and also apply the patch we need?

___

# Mutating Admission Webhooks to the rescue!

[Official documentation](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/)

![](mutatingwebhook.png)


This kind of webhook is intended to be executed during a request to the kubernetes API.

In your case, this will mean that you can run `helm install`, which applies the manifests, and the webhook will inject your patches.

Now, you need to develop the logic for that webhook.

> `Uff... It looks complicated, doesn't it?`
>
> **NOT AT ALL!**

___

# Open Policy Agent!

[Rego language](https://www.openpolicyagent.org/docs/latest/policy-language/) is used by OPA ([Open Policy Agent](https://www.openpolicyagent.org/docs/latest/kubernetes-tutorial/)) to define the policies. In your case, the policy applies to the patches.


![](tools-playground.png)

> `Do you want to see how?`



# The 5 minutes challenge

I propose you make a sample of the feature.

Let's say you need to expose the pod's ports to the host network, but using a mutating admission webhook.

```yaml
[...]
    hostNetwork: true
[...]
```

## Let's get started!

## Requisites

Firstly, clone the repo (https://github.com/antonioberben/examples).
```sh
git clone git@github.com:antonioberben/examples.git
```
And go to the folder.
```sh
cd examples/mutating-admission-webhook
```


Also, you need a namespace where to work.
```sh
kubectl create ns demo-mutate
```

## Create a Certificate

Why? We mentioned above that one of the steps in a kubernetes API request is to execute the `mutating admission controllers`, which runs a request to a given webhook, your mutating application.

That request is intended to be secure. And that is why you need to set up the certificate.

![](tls.png)


There are many scripts to generate the required certificate. You can use this script from github user [@morvencao](https://github.com/morvencao) (Thank you for the script):
```sh
wget https://raw.githubusercontent.com/morvencao/kube-mutating-webhook-tutorial/master/deployment/webhook-create-signed-cert.sh
```

```sh
chmod +x webhook-create-signed-cert.sh
```
```sh
./webhook-create-signed-cert.sh --service mutate-example --namespace demo-mutate --secret mutate-example-tls
```

#### Create the webhook

Now you have everything to set up your mutating admission webhook. But, let me explain them to you, as you apply them:

- ConfigMap: Contains the rego policy. As we said before, [Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) is the language used by Open Policy Agent. In your case, it will define the action to patch the incoming resource definition.

  Copy the content and save it in your computer. Then apply it to your cluster.
  ```sh
  kubectl create configmap mutate-policy -n demo-mutate --from-file=./mutate.rego
  ```

- Deployment: Contains the reference to OPA server official image and it mounts the `mutate.rego` from the previous step and the pair certificate+key you have also created before.

  As you can see, the exposed port is 443 for TLS.

  ```sh
  kubectl apply -f deployment.yaml
  ```
  

- Service: Nothing special from this resource. 
  ```sh
  kubectl apply -f service.yaml
  ```

- MutatingWebhookConfiguration: This configuration lets kubernetes know that a new webhook needs to be triggered when the given conditions are met. In your case, the conditions are:
    - Action: CREATE
    - apiVersion: v1
    - resource: pod
    - matchLabels: "app: mutate-me"


    > **Warning**: You need to do some manual tuning. In the given `mutatingWebhookConfiguration.yaml` you have to add the certificate you created before. To retrieve the value, you run the following command:
    ```sh
    kubectl get secret mutate-example-tls -n demo-mutate -o 'go-template={{index .data "cert.pem"}}' > cert
    ```

    Paste the content where in the `mutatingWebhookConfiguration.yaml` says:
    ```yaml
    [...]
            caBundle: <PASTE HERE THE CERT VALUE>
    [...]
    ```

    Now, apply it:
    ```sh
    kubectl apply -f mutatingWebhookConfiguration.yaml
    ```


> **That is all!**


### Test!
Time to test it. Let's apply some example like `mutateme-application-deployment.yaml`

```
kubectl apply -f mutateme-application-deployment.yaml
```

And let's verify it!

```sh
kubectl get pod -l app=mutate-me -n demo-mutate -o jsonpath='{.items[0].spec.hostNetwork}'
```

> **Ta da!** 

You can see the value is what you have configured in the `mutate.rego`


Now you have your mutating admission webhook up & running.

**5 minutes reading. 5 minutes developing. Done!**
___

# Last Thoughts

I would suggest you play with Open Policy Agent a bit. Why? Because your next challenge will be using Open Policy Agent to perform Authorization for one service.

So, **let’s raise the bet!** In the next article you will be able to put together a service mesh (based on `Istio)`, an Auth Server (based on `keycloak`) and a policy management system (based on `Open Policy Agent`)

But, until the article is ready, you can ping me in twitter [@antonio_berben](https://twitter.com/antonio_berben)


---
## Thanks

Thanks a lot to [@jorgemoralespou](https://twitter.com/jorgemoralespou) for helping me to write the article.

---

## Resources
- https://antonioberben.gitlab.io/website/posts/mutating-admission-webhook-with-opa/
- https://github.com/antonioberben/examples
- https://www.openpolicyagent.org/ 



