---
title: "Platform Engineering Essential Tools"
date: 2024-01-25

tags: ['k8s','opentelemetry', 'observability', 'mesh', 'istio', 'opa']
author: "Antonio Berben"
---

Published in the Solo's official blog:

https://www.solo.io/blog/platform-engineering-essential-tools/