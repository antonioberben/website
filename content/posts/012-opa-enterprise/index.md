---
title: "Enterprise-level policy enforcement with OPA (Open Policy Agent) and Gloo Edge"
date: 2022-04-06

tags: ['k8s', 'opa', 'open policy agent', 'authorization', 'authz', 'abac']
author: "Antonio Berben"
---

Published in the Solo.io's official blog:

https://www.solo.io/blog/opa-open-policy-agent-gloo-edge/