---
title: "Enhancing Access Control: OPA and Gloo Synergyaaaa"
date: 2023-11-09

tags: ['platform engineering', 'k8s','opentelemetry', 'observability', 'mesh', 'istio']
author: "Antonio Berben"
---

Published in the Solo's official blog:

https://www.solo.io/blog/access-control-opa-gloo/