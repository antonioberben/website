---
title: "Draw.io library containing Istio diagrams"
date: 2020-10-28

tags: ['k8s', 'istio']
author: "Antonio Berben"
---

## Istio diagrams

Since I work with Istio, for technical diagrams, I personally like the ones displayed in the official website, don't you?

To contribute with Istio blog or documentation, it is needed to follow the community [guidelines](https://istio.io/latest/about/contribute/).

It is offered how to create [diagrams](https://istio.io/latest/about/contribute/diagrams/). However, they are created for Google Draws.

Since I use [draw.io](https://app.diagrams.net/), I decided to create my own library to quickly create Istio diagrams.

There is a shape with label `Text` which can be customized since it is a mix of multiple `drawio` shapes rather than a static image. The same applies for one with label `Service N`.

Download this [library file](./istio-drawio-library.xml) 

Open it into your `.drawio` project and... ta-da!
