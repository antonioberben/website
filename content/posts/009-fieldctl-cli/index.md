---
title: "Fieldctl, the local development multi k8s environment"
date: 2022-01-04

tags: ['k8s', 'lima', 'vm', 'limavm', 'vcluster', 'k3s']
author: "Antonio Berben"
---

This cli aims to help on operating a local environment. The intention is to wrap some complicated commands which are used to develop in local.

![fieldctl sample](https://raw.githubusercontent.com/antonioberben/fieldctl/main/docs/fieldctl.gif)


Github project: https://github.com/antonioberben/fieldctl