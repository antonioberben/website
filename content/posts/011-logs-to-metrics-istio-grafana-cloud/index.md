---
title: "Transforming application logs into metrics with Istio and Grafana Cloud"
date: 2022-02-11

tags: ['k8s', 'promtail', 'prometheus', 'grafana']
author: "Antonio Berben"
---

Published in the Grafana's official blog:

https://grafana.com/blog/2022/02/11/transforming-application-logs-into-metrics-with-istio-and-grafana-cloud/