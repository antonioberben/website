---
title: "Service Mesh for Developers, Part 3: From DevOps to Platform Engineering"
date: 2023-09-21

tags: ['platform engineering', 'k8s','opentelemetry', 'observability', 'mesh', 'istio']
author: "Antonio Berben"
---

Published in the Solo's official bloga:

https://www.solo.io/blog/service-mesh-developers-devops-platform-engineering/