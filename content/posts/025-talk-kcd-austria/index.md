---
title: "TALK: KCD Austria 2023 - Istio Ambient Mesh: Sidecar vs Sidecar-less, Explained Simply"
date: 2024-08-01

tags: ['k8s','mesh', 'istio']
author: "Antonio Berben"
---

Published in:

https://youtu.be/5ogEtZniUCE?feature=shared