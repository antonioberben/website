---
title: "Don't forget these commands"
date: 2020-02-20

tags: ['k8s']
author: "Antonio Berben"
---

How did we do this thing... ? I always forget

<!--more-->

## Deleting namespace stuck at "Terminating" state

This might be probably due to finalizers. Run following:


```
kubectl get namespace "my-namespace" -o json \                                         
            | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/" \
            | kubectl replace --raw /api/v1/namespaces/my-namespace/finalize -f -
```

___

## Get a value from a resource with dots in the key using jsonpath

```
kubectl get secret resource -o 'go-template={{index .data "tls.crt"}}'
```

## Get the value of a certificate

```
kubectl get secret resource -n namespace -o json | jq -r '.data["cert.pem"]' | base64 --decode | openssl x509 -in - -text -noout
```

## Port-forward assigning PID to a file
```
kubectl port-forward svc/<service> -n <namespace>  8080 --address 0.0.0.0 &> /dev/null &
echo $! > my.pid
```
