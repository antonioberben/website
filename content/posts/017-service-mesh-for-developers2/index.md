---
title: "Service Mesh for Developers, Part 2: Testing in Production"
date: 2023-08-11

tags: ['k8s','opentelemetry', 'observability', 'mesh', 'istio']
author: "Antonio Berben"
---

Published in the Solo's official bloga:

https://www.solo.io/blog/service-mesh-developers-testing-production/