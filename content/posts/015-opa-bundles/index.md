---
title: "Enhancing Access Control: OPA and Gloo Platform Synergy"
date: 2023-09-01

tags: ['k8s','opa', 'istio']
author: "Antonio Berben"
---

Published in the Solo's official blog:

https://www.solo.io/blog/service-mesh-for-developers-exploring-the-power-of-observability-and-opentelemetry/