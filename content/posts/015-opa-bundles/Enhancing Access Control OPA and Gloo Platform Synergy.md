Antonio Berben,   Tyler Schade | November 09, 2023

In the dynamic realm of cloud-native security, where the intention is to achieve authorization at scale, discover the shift from traditional [role-based access control (RBAC)](https://en.wikipedia.org/wiki/Role-based_access_control) to the flexible domain of [attribute-based access control (ABAC)](https://en.wikipedia.org/wiki/Attribute-based_access_control).

In this post, we’ll explore the power of the [Open Policy Agent (OPA)](https://www.openpolicyagent.org/) and its integration with the Gloo Platform, providing a comprehensive approach to authorization management in the cloud-native era.

## The Origins of Authorization

To comprehend the historical context, let’s explore the origins of authorization.

Authorization seeks a straightforward answer: “Am I permitted to perform this action? Am I authorized?” Within the application layer, security involves two key phases:

-   Authentication: Who is the user?
-   Authorization: Can the user perform this action?

Traditionally, software architects relied on a single attribute, the user’s role, to grant or deny permissions. This model, called role-based access control (RBAC), classified users as admins, users, employees, editors, and more.

![](https://www.solo.io/wp-content/uploads/2023/10/image4-1-274x300.png)

However, roles are just one facet of user attributes. The concept of attribute-based access control (ABAC) emerged to enable access control based on any attribute, be it inherent or derived from various sources, providing a more versatile approach to authorization.

### XACML

For a long time, a language known as [Extensible Access Control Markup Language (XACML)](https://en.wikipedia.org/wiki/XACML) was quite popular in various systems. It was like a powerful toolbox, allowing systems to handle ABAC. XACML is like the building blocks of today’s authorization methods.

![](https://www.solo.io/wp-content/uploads/2023/10/image8-1-300x295.png)

The components are:

-   Policy Information Point (PIP): Attempts to resolve information required by the policy to execute. The PIP is typically composed of many attribute value providers.
-   Policy Administration Point (PAP): The point that manages access authorization policies.
-   Policy Decision Point (PDP): The point where access requests are evaluated against authorization policies before issuing access decisions.
-   Policy Enforcement Point (PEP): The point where user access requests are intercepted to make a decision request to the PDP.

## Cloud Native Authorization: OPA

As the world shifted toward cloud-native solutions, the older XACML-based tools became outdated. In their place, something new came along: the [Open Policy Agent](https://www.openpolicyagent.org/).

Building on the components we talked about earlier, it sets up this kind of structure:

![](https://www.solo.io/wp-content/uploads/2023/10/image5-2-291x300.png)

### OPA Strengths: ABAC Flexibility Compared to RBAC

Let’s say a company wants to run a special holiday promotion during Christmas, but only for its users in the European Union. This situation requires ABAC because it depends on a specific attribute (location and time) rather than a general role.

In the world of cloud-native systems, an authorization method should follow certain principles (like the 12-factor approach). Open Policy Agent (OPA) does this by separating the Policy Administration Point (PAP) from other components. This separation allows you to store policies in repositories like [Amazon S3](https://aws.amazon.com/s3/), [Google Cloud Storage](https://cloud.google.com/storage), [Minio,](https://min.io/) or [Styra DAS](https://www.styra.com/styra-das/), making them easily accessible and distributable across regions.

![](https://www.solo.io/wp-content/uploads/2023/10/image3-1-300x140.png)

In this setup, you can replicate policies globally by taking advantage of the simplicity of S3 bucket replication, resulting in a more efficient and scalable architecture.

![](https://www.solo.io/wp-content/uploads/2023/10/image6-1-300x111.png)

S3 low-latency access and rapid data transfer allow globally distributed infrastructure and data replication mechanisms. So we can synchronize PAPs across regions:

![](https://www.solo.io/wp-content/uploads/2023/10/image7-2-300x102.png)

## OPA in Gloo Platform

It’s worth noting that a good starting point for understanding how OPA integrates with service meshes is by looking at [fine-grained service authorizations with Istio and OPA](https://www.solo.io/blog/fine-grained-service-authorizations-istio-opa/). [Peter Jausovec](https://www.linkedin.com/in/pjausovec/), one of our colleagues, has written an interesting post with a [YouTube recording](https://www.youtube.com/watch?v=EnckV6lyre8) that provides a great introduction to the topic.

After that, one can expect Gloo Platform to integrate OPA to extend its already existing RBAC capabilities. And not just in the Gloo Platform, as we show in a preview post, where we explained [OPA with Gloo Edge](https://www.solo.io/blog/opa-open-policy-agent-gloo-edge/).

Now, let’s focus on the Gloo Platform and the three methods it employs for OPA integration as an ABAC component.

### OPA As Sidecar

Gloo Platform introduced a new feature: they deployed OPA [as a “sidecar”](https://docs.solo.io/gloo-mesh-enterprise/main/policies/external-auth/opa/opa-sidecar/) alongside their well-proven ExtAuth Service, which is like the guardian of access. This ExtAuth Service already had several plugins, including OPA, with its Rego rules stored as ConfigMaps. However, as the platform grew and we expanded to multiple clusters in various regions, the idea of storing policies locally didn’t make sense anymore.

So, we made a smart move: we set up a separate OPA server next to the existing ExtAuth service. This separation allowed users to leverage OPA bundles and design solutions, as shown earlier and made things more efficient and scalable. 

Here’s how it’s laid out:

## ![](https://www.solo.io/wp-content/uploads/2023/10/image10-1-300x165.png)

### OPA As Remote Server

This mode ([Bring your own OPA server](https://docs.solo.io/gloo-mesh-enterprise/main/policies/external-auth/opa/opa-byo/)) is ideal when you already have an OPA server up and running in your system and wish to continue using it.

The architectural layout for this mode appears as follows:

![](https://www.solo.io/wp-content/uploads/2023/10/image11-1-300x152.png)

In this setup, your existing OPA server remains at the core, seamlessly integrated into the system to handle authorization tasks efficiently.

### OPA As ConfigMap

The final mode ([OPA with Rego rules in config maps](https://docs.solo.io/gloo-mesh-enterprise/main/policies/external-auth/opa/opa-rego-cm/)) is the initial implementation in the Gloo Platform, and it offers a straightforward way to manage authorization policies.

This particular mode is a great fit for:

1.  Single cluster environments.
2.  Environments where it makes sense to store policies within each cluster.
3.  Teams who find a global policy enforcement mechanism to be overly complex for their platform’s needs.

Here’s what the architecture for this mode looks like:

![](https://www.solo.io/wp-content/uploads/2023/10/image1-2-300x196.png)

This configuration emphasizes simplicity, making it well-suited for specific scenarios where a more straightforward approach to handling authorization policies is preferred.

## Platform Policies Vs. Application Policies

Within the Istio ecosystem, it’s possible to add OPA as a sidecar alongside your application. While this might seem tempting, it’s generally considered poor practice.

It’s important to note that this deployment method of OPA is best suited when you’re focusing on authorization policies at the application level. These policies are often specific to individual applications and can be standardized using Rego rules. Many development teams employ OPA as an additional agent alongside the application to enhance its capabilities.

![](https://www.solo.io/wp-content/uploads/2023/10/image9-1-300x170.png)

However, we’re looking at a different scenario when we shift our attention to the Gloo Platform and its OPA deployments. Here, the emphasis is on the platform as a whole, not individual applications. The rules enforced by this OPA deployment are managed by the Platform Team and have the capacity to impact the entire platform, independently of the authorization mechanisms in place for individual applications.

## ![](https://www.solo.io/wp-content/uploads/2023/10/image2-2-300x154.png)

## Learn More in Our Workshop

[In this hands-on workshop](https://github.com/solo-io/solo-cop/tree/main/workshops/opa-sidecar-externalauth), you’ll learn to deploy OPA as a sidecar for Gloo Platform’s ExtAuth Service.

The process is quite simple. We’ll save the policy in Google Storage, and you’ll set up OPA to fetch this policy bundle from the repository. We’ll also deploy the widely-used [httpbin](https://httpbin.org/) application for testing purposes.

Finally, you’ll put your newfound knowledge to the test by checking how the rules you’ve configured in the policy perform.

[Repository for the workshop](https://github.com/solo-io/solo-cop/tree/main/workshops/opa-sidecar-externalauth)

## Experience Dynamic Policy Management

OPA’s integration with the Gloo Platform empowers a versatile approach to authorization, where attribute-based access control (ABAC) takes the lead, enabling dynamic policy management. Whether deployed as a sidecar, a remote server, or a ConfigMap, OPA enriches cloud-native security and scalability while maintaining simplicity in policy administration. This transformation paves the way for secure, efficient, and adaptable authorization in modern, distributed applications.

[Explore more about how Gloo Platform secures cloud applications and simplifies API management.](https://www.solo.io/products/gloo-platform/)