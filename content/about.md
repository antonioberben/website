+++
title = "About Me"
date = "2020-01-01"
+++

<div style="text-align:center"><img src="face.png" alt="alt text" width="100"></div>

I am a **Pre-sales Engineer** for EMEA and LATAM, with many years of hands-on experience in **Cloud Native** architectures, legacy system migrations, **agile** teams transformations.

My interests are **Service Mesh, kubernetes, Zero Trust, Cloud Native and containers**.

During my free time I develop my own solution for trading deployed on a rapstberry PI, and recently, a new idea using AI that I cannot share with you yet ;)

> Note about the above trading: In the end just make it simple. Use [Freqtrade](https://www.freqtrade.io/en/stable/) and the Telegram bot :)

I organize CNCF meetups with some freinds in the industry. You can find us here: [CNCF Iberia](https://community.cncf.io/cloud-native-iberia/).

As well, I am one of the organizers of [KCD Spain](https://community.cncf.io/cloud-native-iberia/) and [KCD Madrid](https://github.com/cncf/kubernetes-community-days/issues/539). Don't miss the next one!

Besides, I am continuously learning new technologies to apply in my day-by-day work.

I am a big fan of **Istio** and **Kubernetes**. I have been working with them for the last 6 years. I have been involved in the community and I have been giving talks and webinars about them. Please, don't hesiteta to [read my posts and watch my talks](https://antonioberben.gitlab.io/website/posts/).

<!--
You can find my journey described in this [article](../posts/journey-to-conquer-the-world/) where I explain my point of view about Agile and Devops. Two of the biggest buzzwords nowadays.
-->

I am quite active in Slack (@antonio_berben) at following workspaces:
- [CNCF](https://cloud-native.slack.com/)
- [Kubernetes](https://slack.k8s.io/)
- [Istio](https://slack.istio.io/)

Or in LinkedIn [@antonio-berben](https://www.linkedin.com/in/antonio-berben/)
Or in Twitter [@antonio_berben](https://twitter.com/antonio_berben)

## Certifications

- [Certified Kubernetes Application Developer](https://www.credly.com/badges/8df17b20-46d3-4c58-b75e-f1587a4ef8ad?source=linked_in_profile)

- [IBM Cloud Garage Architect](https://www.youracclaim.com/badges/61a8ec8a-ddcc-49ef-8c2e-deae79e020f8/linked_in_profile)

- [IBM Cloud Private - Continuous Integration/Continuous Delivery Pipelines](https://www.youracclaim.com/badges/4c433ff6-4162-4a35-8bdb-ccb1f8d5f91a/linked_in_profile)
